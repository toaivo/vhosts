FROM httpd

ARG USERID

RUN usermod -u ${USERID} www-data && \
    mkdir -p /usr/local/apache2/docs/vhost.example.com

COPY httpd.conf        /usr/local/apache2/conf
COPY httpd-vhosts.conf /usr/local/apache2/conf/extra
COPY httpd-ssl.conf /usr/local/apache2/conf/extra

COPY example.crt /usr/local/apache2/conf/example.crt
COPY example.key /usr/local/apache2/conf/example.key
COPY vhost.crt /usr/local/apache2/conf/vhost.crt
COPY vhost.key /usr/local/apache2/conf/vhost.key

EXPOSE 80
EXPOSE 443
