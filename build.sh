#!/bin/bash

set -xv
USERID=`id -u`
docker build --build-arg USERID=$USERID -t vhosts .
